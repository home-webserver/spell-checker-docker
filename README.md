# Spell Checking in Docker

This is a simple Alpine image to be used in CI of LaTeX projects.

It includes tools like `aspell`, with an english dictionary and `texcount` for word counting.

## In GitLab CI

In GitLab CI just add a step that runs with the image

```yaml
word_count_and_spelling_errors:
  image: registry.gitlab.com/home-webserver/spell-checker-docker:latest
  script:
    - echo "Spelling errors: $(for f in **/*.tex; do aspell -t -a $f; done | grep ^\& -c)"
    - texcount **/*.tex | grep Total -A 5 | grep -e "Words in text" | echo
```

or you can store the results and parse them however you like.
